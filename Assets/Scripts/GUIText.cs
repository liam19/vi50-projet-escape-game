﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIText : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject text, image;
    private readonly float appearSpeed = 0.5f;
    private bool canAppear = false, appeared = false;
    private Color textColor, imageColor;

    public void Start()
    {
        StartCoroutine(WaitAppear());
        textColor = text.GetComponent<Text>().color;
        imageColor = image.GetComponent<Image>().color;
    }

    public void Update()
    {
        if (!appeared)
        {
            if (canAppear)
            {
                text.GetComponent<Text>().color = new Color(textColor.r, textColor.g, textColor.b, textColor.a + Time.deltaTime * appearSpeed);
                image.GetComponent<Image>().color = new Color(imageColor.r, imageColor.g, imageColor.b, imageColor.a + Time.deltaTime * appearSpeed);
                textColor = text.GetComponent<Text>().color;
                imageColor = image.GetComponent<Image>().color;
                if (textColor.a > 0.7f)
                {
                    appeared = true;
                }
            }
        }
    }

    private IEnumerator WaitAppear()
    {
        yield return new WaitForSeconds(10f);
        canAppear = true;
    }
}
