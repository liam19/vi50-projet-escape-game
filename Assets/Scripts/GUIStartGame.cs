﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIStartGame : MonoBehaviour
{
    OVRInput.Controller controllerOn = OVRInput.Controller.None;

    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch) && controllerOn == OVRInput.Controller.RTouch)
        {
            StartGame();
        }
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch) && controllerOn == OVRInput.Controller.LTouch)
        {
            StartGame();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.transform.parent.transform.parent.transform.parent.name == "CustomHandRight")
        {
            controllerOn = OVRInput.Controller.RTouch;
        }
        else if (other.gameObject.transform.parent.transform.parent.transform.parent.name == "CustomHandLeft")
        {
            controllerOn = OVRInput.Controller.LTouch;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        controllerOn = OVRInput.Controller.None;
    }

    void StartGame()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }
}
