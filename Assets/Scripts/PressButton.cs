﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressButton : MonoBehaviour
{
    private Animator anim;
    private bool quit = false;
    private AudioSource mAudio;

    public void Start()
    {
        anim = GetComponent<Animator>();
        mAudio = GetComponent<AudioSource>();
    }
	
	public void Update() {
		if (quit) {
			if (anim.GetCurrentAnimatorStateInfo(0).length <= anim.GetCurrentAnimatorStateInfo(0).normalizedTime) {
				Application.Quit();
			}
		}
	}

    private void OnTriggerEnter(Collider other)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime)
        {
            return;
        }
 
        if (other.gameObject.transform.parent.transform.parent.transform.parent.name == "CustomHandRight")
        {
            anim.Play("push", 0, 0);
            mAudio.Play(0);
            StartCoroutine(Vibration(OVRInput.Controller.RTouch));
            quit = true;
        }
    }

    private IEnumerator Vibration(OVRInput.Controller controller)
    {
        OVRInput.SetControllerVibration(0.5f, 0.5f, controller);
        yield return new WaitForSeconds(0.15f);
        OVRInput.SetControllerVibration(0, 0, controller);
    }
}
