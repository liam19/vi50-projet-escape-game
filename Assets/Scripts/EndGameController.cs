using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameController : MonoBehaviour
{
    private DigicodeController digicode;

    // Start is called before the first frame update
    void Start()
    {
        digicode = GetComponent<DigicodeController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (digicode.activated)
        {
            SwitchToEndScene();
        }
    }

    private void SwitchToEndScene()
    {
        SceneManager.LoadScene("End", LoadSceneMode.Single);
    }
}
