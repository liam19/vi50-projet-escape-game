﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holder : MonoBehaviour
{
    public bool IsActivated { get; private set; } = false;
    public string objectCode;

    /*public Material FuseFilamentOn;*/

    public GameObject objectHolded;
    private HolderManager manager;
    public Material fuseFilamentOn;

    private void Start()
    {
        manager = transform.parent.gameObject.GetComponent<HolderManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!IsActivated)
        {
            ObjectToHold hold = other.GetComponent<ObjectToHold>();

            if (hold != null)
            {
                if (hold.objectCode == objectCode)
                {
                    if (GameObject.Find("CustomHandRight").GetComponent<Grabber>().objectInHand == other.gameObject)
                    {
                        GameObject.Find("CustomHandRight").GetComponent<Grabber>().ReleaseObject();
                    }
                    else if (GameObject.Find("CustomHandLeft").GetComponent<Grabber>().objectInHand == other.gameObject)
                    {
                        GameObject.Find("CustomHandLeft").GetComponent<Grabber>().ReleaseObject();
                    }

                    other.transform.SetParent(transform);
                    other.transform.localPosition = Vector3.zero;
                    other.transform.localRotation = Quaternion.Euler(Vector3.zero);
                    other.GetComponent<Rigidbody>().isKinematic = true;

                    objectHolded = other.gameObject; // to access data if needed

                    if (hold.objectCode == "Fuse")
                    {
                        other.transform.GetChild(2).GetComponent<Renderer>().material = fuseFilamentOn;
                    }
                    IsActivated = true;

                    if (hold.objectCode != "Disk")
                    {
                        // disks need to still be grabbable after insertion
                        other.GetComponent<Grabbable>().grabbable = false;
                    }
                    GetComponent<AudioSource>().Play();

                    manager.UpdateManagerStatus();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ObjectToHold hold = other.GetComponent<ObjectToHold>();
        if (hold != null)
        {
            if (hold.objectCode == objectCode)
            {
                IsActivated = false;
                objectHolded = null;
                manager.UpdateManagerStatus();
            }
        }
    }
}
