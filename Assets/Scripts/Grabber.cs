﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour
{
    // identifying objects

    public GameObject CollidingObject;
    public GameObject objectInHand;
    public OVRInput.Controller thisHand;
    private MoveSystem moveSystem;

    // trigger functions after adding trigger zones to controllers and adding script to controllers

    private void Start()
    {
        moveSystem = GameObject.Find("OVRCameraRig").GetComponent<MoveSystem>();
    }

    public void OnTriggerEnter(Collider other) //picking up objects with rigidbodies
    {
        Grabbable script = other.gameObject.GetComponent<Grabbable>();
        if (script != null)
        {
            if (script.grabbable)
                CollidingObject = other.gameObject;
        }
    }

    public void OnTriggerExit(Collider other) // releasing those objects with rigidbodies
    {
        CollidingObject = null;
    }

    void Update() // refreshing program confirms trigger pressure and determines whether holding or releasing object
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, thisHand) && CollidingObject)
        {
            GrabObject();
        }

        if (OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, thisHand) && objectInHand)
        {
            ReleaseObject();
        }

        if (OVRInput.GetDown(OVRInput.Button.One, thisHand) && objectInHand)
        {
            if (objectInHand.tag == "UV")
                objectInHand.GetComponent<UVFlashlightController>().switchFlashlight();
            if (objectInHand.tag == "SoluceLight")
                objectInHand.GetComponentInChildren<Projector>().enabled = !objectInHand.GetComponentInChildren<Projector>().enabled;
            if (objectInHand.tag == "Recorder")
            {
                if (!objectInHand.GetComponent<AudioSource>().isPlaying)
                    objectInHand.GetComponent<AudioSource>().Play();
            }
        }
    }

    public void GrabObject() //create parentchild relationship between object and hand so object follows hand
    {
        objectInHand = CollidingObject;

        FloatingObject floating = objectInHand.GetComponent<FloatingObject>();

        if (floating != null)
            if (floating.enabled)
                floating.enabled = false;

        objectInHand.transform.SetParent(transform);
        objectInHand.GetComponent<Rigidbody>().isKinematic = true;
    }

    public void ReleaseObject() //removing parentchild relationship so you drop the object
    {
        if (objectInHand != null)
        {
            objectInHand.transform.SetParent(GameObject.Find("Station").transform);
            objectInHand.GetComponent<Rigidbody>().isKinematic = false;

            moveSystem.FixRotation();

            if (thisHand == OVRInput.Controller.RTouch)
            {
                objectInHand.GetComponent<Rigidbody>().AddForce(-moveSystem.deltaPositionR * 1500);
                objectInHand.GetComponent<Rigidbody>().AddTorque(moveSystem.deltaRotationR / 10);
            }
            if (thisHand == OVRInput.Controller.LTouch)
            {
                objectInHand.GetComponent<Rigidbody>().AddForce(-moveSystem.deltaPositionL * 1500);
                objectInHand.GetComponent<Rigidbody>().AddTorque(moveSystem.deltaRotationL / 10);
            }
          
            objectInHand = null;
        }
    }
}
