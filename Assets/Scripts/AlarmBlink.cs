﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmBlink : MonoBehaviour
{
    public bool isOn = true;
    private Light lght;
    // Start is called before the first frame update
    void Start()
    {
        lght = GetComponentInChildren<Light>();
        if (isOn)
        {
            StartAlarm();
        }
    }

    private IEnumerator BlinkLight()
    {
        while (isOn)
        {
            lght.enabled = !lght.enabled;
            yield return new WaitForSeconds(0.65f);
        }
    }

    public void StopAlarm()
    {
        StopAllCoroutines();
        lght.enabled = false;
        GetComponent<AudioSource>().Stop();
        isOn = false;
    }

    public void StartAlarm()
    {
        isOn = true;
        GetComponent<AudioSource>().Play();
        StartCoroutine(BlinkLight());
    }
}
