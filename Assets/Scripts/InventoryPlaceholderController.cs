using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryPlaceholderController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Grabbable>())
        {
            Grabber rightHand = GameObject.Find("CustomHandRight").GetComponent<Grabber>();
            Grabber leftHand = GameObject.Find("CustomHandLeft").GetComponent<Grabber>();

            if (rightHand.objectInHand == other.gameObject || leftHand.objectInHand == other.gameObject) return;

            other.transform.SetParent(transform);
            other.GetComponent<Rigidbody>().isKinematic = true;

            other.GetComponent<FloatingObject>().enabled = false;
        }
    }
}
