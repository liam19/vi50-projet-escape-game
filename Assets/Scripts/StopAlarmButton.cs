﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAlarmButton : MonoBehaviour
{
    public AlarmBlink alarmBlink;
    public RectTransform text;
    Animator anim;
    public GameObject canvas;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime)
        {
            return;
        }
        if (alarmBlink.isOn)
            alarmBlink.StopAlarm();
        anim.Play("push", 0, 0);
        GetComponent<AudioSource>().Play();
        if (canvas != null)
            Destroy(canvas);
    }
}
