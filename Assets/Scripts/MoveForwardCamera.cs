﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForwardCamera : MonoBehaviour
{

// Update is called once per frame
void Update()
    {
        if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).y > 0.85f)
        {
            
            //transform.position += Camera.main.transform.forward * Time.deltaTime;
            transform.Translate(Camera.main.transform.forward / 100);
        }
    }
}
