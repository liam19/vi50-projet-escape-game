﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screamer : MonoBehaviour
{
    public AlarmBlink alarm;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GetComponent<BoxCollider>().enabled = false;
            GetComponent<AudioSource>().Play();
            alarm.StartAlarm();
            StartCoroutine(ShakeOnce(other.gameObject, 8f, 100f, 0.04f));
        }
    }

    private IEnumerator ShakeOnce(GameObject player, float duration, float speed, float roughness) { 

        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            Vector3 position = player.transform.position;
            position.y += Mathf.Sin(Time.time * speed) * roughness;
            position.x += Mathf.Sin(Time.time * speed * 0.58f + 3.14f) * roughness;
            player.transform.position = position;
            elapsed += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
    }   
}
