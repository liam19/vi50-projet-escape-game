﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSystem : MonoBehaviour
{
    private GameObject playerBody;
    private WallInHand leftHandScript;
    private WallInHand rightHandScript;
    public GameObject leftHand, rightHand;
    private Transform leftAnchor, rightAnchor;

    private Vector3 velocityVector = new Vector3(0, 0, 0);
    private Vector3 lastPositionR = Vector3.zero;
    public Vector3 deltaPositionR = Vector3.zero;
    private Vector3 lastPositionL = Vector3.zero;
    public Vector3 deltaPositionL = Vector3.zero;

    private Vector3 lastRotationR = Vector3.zero;
    public Vector3 deltaRotationR = Vector3.zero;
    private Vector3 lastRotationL = Vector3.zero;
    public Vector3 deltaRotationL = Vector3.zero;

    private GameObject structure;

    public float speed = 100f;
    private bool grabedRight = false, grabedLeft = false;

    private GameObject collidingObjectLeft, collidingObjectRight;
    private GameObject lastCollObjLeft, lastCollObjRight;

    // Start is called before the first frame update
    void Start()
    {
        playerBody = gameObject;
        rightAnchor = rightHand.transform.parent;
        leftAnchor = leftHand.transform.parent;

        rightHandScript = rightHand.GetComponent<WallInHand>();
        leftHandScript = leftHand.GetComponent<WallInHand>();
        structure = GameObject.Find("STRUCTURE");

        List<GameObject> children = new List<GameObject>();
        GetAllChildren(structure, children);

        foreach (GameObject child in children)
        {
            if (child.GetComponent<BoxCollider>() != null)
            {
                child.tag = "Wall";
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        collidingObjectLeft = leftHandScript.CollidingObject;
        collidingObjectRight = rightHandScript.CollidingObject;

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        //
        //playerBody.transform.Rotate(new Vector3(0, -0.014f, 0) * 100 * speed * Time.deltaTime);

        if (!(grabedLeft && grabedRight))
        {
            //playerBody.transform.rotation = Quaternion.identity;
        }
        else { 
            
        }

        if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).y > 0.85f)
        {
            //transform.position += Camera.main.transform.forward * Time.deltaTime;
            velocityVector = Camera.main.transform.forward / 50;
        }

        if (OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch).y < -0.85)
        {
            velocityVector = Vector3.zero;
        }
        //if (OVRInput.Get(OVRInput.Button.One))
        //{
        //    velocityVector += rightHand.transform.forward / 400;
        //}
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch) && collidingObjectRight)
        {
            grabedRight = true;
            rightHand.transform.SetParent(structure.transform.parent);
            lastCollObjRight = collidingObjectRight;
        }
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch) && collidingObjectLeft)
        {
            grabedLeft = true;
            leftHand.transform.SetParent(structure.transform.parent);
            lastCollObjLeft = collidingObjectLeft;
        }

        if (OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch) && grabedRight)
        {
            grabedRight = false;
            rightHand.transform.SetParent(rightAnchor);
            rightHand.transform.localPosition = Vector3.zero;
            rightHand.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -90f));
            rightHandScript.CollidingObject = lastCollObjRight;
        }
        if (OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch) && grabedLeft)
        {
            grabedLeft = false;
            leftHand.transform.SetParent(leftAnchor);
            leftHand.transform.localPosition = Vector3.zero;
            leftHand.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 90f));
            leftHandScript.CollidingObject = lastCollObjLeft;
        }

        if (grabedRight && !grabedLeft)
        {
            velocityVector = deltaPositionR * speed * Time.deltaTime;
        }
        else if (grabedLeft && !grabedRight)
        {
            velocityVector = deltaPositionL * speed * Time.deltaTime;
        }
        else if (grabedLeft && grabedRight)
        {
            //playerBody.transform.Rotate(new Vector3(deltaPositionR.x, deltaPositionR.z, deltaPositionR.y) * 20 * speed * Time.deltaTime);
            //playerBody.transform.Rotate(rightAnchor.localPosition - playerBody.transform.position + leftAnchor.localPosition - playerBody.transform.position, deltaPositionR.magnitude);
        }

        playerBody.GetComponent<CharacterController>().Move(velocityVector);
        //playerBody.GetComponent<Rigidbody>().velocity = velocityVector;
    }

    void FixedUpdate()
    {
        lastPositionL = leftAnchor.localPosition;
        lastPositionR = rightAnchor.localPosition;
        lastRotationR = rightAnchor.localRotation.eulerAngles;
        lastRotationL = leftAnchor.localRotation.eulerAngles;
    }

    private void LateUpdate()
    {
        deltaPositionL = lastPositionL - leftAnchor.localPosition;
        deltaPositionR = lastPositionR - rightAnchor.localPosition;
        deltaRotationR = rightAnchor.localRotation.eulerAngles - lastRotationR;
        deltaRotationL = leftAnchor.localRotation.eulerAngles - lastRotationL;
    }

    private List<GameObject> GetAllChildren(GameObject go, List<GameObject> gameObjects)
    {
        if (go == null)
        {
            return gameObjects;
        }

        gameObjects.Add(go);

        if (go.transform.childCount != 0) {
            foreach (Transform child in go.transform)
            {
                if (null == child)
                {
                    continue;
                }
                GetAllChildren(child.gameObject, gameObjects);
            }
        }

        return gameObjects;
    }

    public void FixRotation()
    {
        //Problem whith quaternion convertion : when we have something above 180, , for example 350, we don't want value to be 350, but -10
        if (deltaRotationR.x > 180)
            deltaRotationR.x = -(360 - deltaRotationR.x);
        if (deltaRotationR.y > 180)
            deltaRotationR.y = -(360 - deltaRotationR.y);
        if (deltaRotationR.z > 180)
            deltaRotationR.z = -(360 - deltaRotationR.z);

        if (deltaRotationL.x > 180)
            deltaRotationL.x = -(360 - deltaRotationL.x);
        if (deltaRotationL.y > 180)
            deltaRotationL.y = -(360 - deltaRotationL.y);
        if (deltaRotationL.z > 180)
            deltaRotationL.z = -(360 - deltaRotationL.z);
    }
}
