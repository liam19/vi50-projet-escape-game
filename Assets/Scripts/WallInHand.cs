﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallInHand : MonoBehaviour
{
    public GameObject CollidingObject { get; set; } = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Wall")
        {
            CollidingObject = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (CollidingObject)
        {
            CollidingObject = null;
        }
    }


}
