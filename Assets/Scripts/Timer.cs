﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public int minutes;
    private int minutesLeft;
    private int secondLeft;
    private bool timerOn = true;

    public List<Text> timerObjects;

    public void Awake()
    {
        minutesLeft = minutes;
        secondLeft = 0;
        StartCoroutine(UpdateTime());
    }

    private IEnumerator UpdateTime()
    {

        while (timerOn && (minutesLeft > 0 || secondLeft > 0))
        {
            secondLeft--;
            if (secondLeft < 0)
            {
                secondLeft = 59;
                minutesLeft--;
            }
            if (secondLeft > 9)
            {
                foreach (Text txt in timerObjects)
                {
                    txt.text = "" + minutesLeft + ":" + secondLeft;
                }
            }
            else
            {
                foreach (Text txt in timerObjects)
                {
                    txt.text = "" + minutesLeft + ":0" + secondLeft;
                }
            }
           
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene("Menu");
    }

    public void StopTimer()
    {
        timerOn = false;
    }
}
