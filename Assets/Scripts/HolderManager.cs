﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderManager : MonoBehaviour
{
    public Holder[] holders;
    public bool IsActivated { get; private set; } = false;

    public void UpdateManagerStatus()
    {
        IsActivated = true;

        // if every holder is activated, then manager is activated
        foreach (Holder holder in holders)
        {
            if (!holder.IsActivated)
            {
                IsActivated = false;
                break;
            }
        }
    }
}
