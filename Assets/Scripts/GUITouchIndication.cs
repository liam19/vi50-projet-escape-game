﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUITouchIndication : MonoBehaviour
{
    public Material touchMat, triggerMat;
    public bool canAppear = false, appeared = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitAppear());
        touchMat.color = new Color(touchMat.color.r, touchMat.color.g, touchMat.color.b, 0);
        triggerMat.color = new Color(triggerMat.color.r, triggerMat.color.g, triggerMat.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (!appeared)
        {
            if (canAppear)
            {
                Color lastTouchColor = touchMat.color, lastTriggerColor = triggerMat.color;
                touchMat.color = new Color(lastTouchColor.r, lastTouchColor.g, lastTouchColor.b, lastTouchColor.a + Time.deltaTime);
                triggerMat.color = new Color(lastTriggerColor.r, lastTriggerColor.g, lastTriggerColor.b, lastTriggerColor.a + Time.deltaTime);

                if (touchMat.color.a >= 0.7f)
                {
                    appeared = true;
                }
            }
        }
    }

    private IEnumerator WaitAppear()
    {
        yield return new WaitForSeconds(15f);
        canAppear = true;
    }
}
