﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedTuto : MonoBehaviour
{
    public GameObject firstFloppyToTake;
    public GameObject recorderToTake;
    public GameObject floppyCanvas;
    public GameObject recorderTakeCanvas;
    public Holder holder;
    private bool firstFloppyTook = false;
    private bool recorderTook = false;
    public Grabber grabLeft, grabRight;
    public GameObject diskReaderCanvas;
    public GameObject useCanvasL, useCanvasR;

    // Update is called once per frame
    void Update()
    {

        if (ReferenceEquals(grabLeft.objectInHand, firstFloppyToTake) || ReferenceEquals(grabRight.objectInHand, firstFloppyToTake))
        {
            
            Destroy(floppyCanvas);
            diskReaderCanvas.SetActive(true);
            firstFloppyTook = true;
        }
        if (firstFloppyTook)
        {
            if (ReferenceEquals(holder.objectHolded, firstFloppyToTake))
            {
                Destroy(diskReaderCanvas);
            }
        }

        if (!recorderTook && (ReferenceEquals(grabLeft.objectInHand, recorderToTake) || ReferenceEquals(grabRight.objectInHand, recorderToTake)))
        {
            Destroy(recorderTakeCanvas);
            recorderTook = true;
            if (ReferenceEquals(grabLeft.objectInHand, recorderToTake))
            {
                useCanvasL.SetActive(true);
            }
            else
            {
                useCanvasR.SetActive(true);
            }
        }

        if (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch) || OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.LTouch))
        {
            if (ReferenceEquals(grabLeft.objectInHand, recorderToTake) || ReferenceEquals(grabRight.objectInHand, recorderToTake)) {
                if (recorderTook)
                {
                    Destroy(useCanvasL);
                    Destroy(useCanvasR);
                }
            }
        }
    }
}
