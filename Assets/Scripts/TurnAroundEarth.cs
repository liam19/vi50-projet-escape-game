﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAroundEarth : MonoBehaviour
{
    public GameObject target;
    public float turnSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(target.transform.position, Vector3.up, turnSpeed * Time.deltaTime);

    }
}
