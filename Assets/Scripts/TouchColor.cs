﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchColor : MonoBehaviour
{
    public Material mat;
    private bool up = true;
    private readonly short changeSpeed = 2;

    private void Start()
    {
    }

    void Update()
    {
        if (mat.color.a >= 0.7f) {
            if (up)
                mat.color = new Color(mat.color.r + Time.deltaTime * changeSpeed, mat.color.g + Time.deltaTime * changeSpeed, mat.color.b + Time.deltaTime * changeSpeed);
            else
                mat.color = new Color(mat.color.r - Time.deltaTime * changeSpeed, mat.color.g - Time.deltaTime * changeSpeed, mat.color.b - Time.deltaTime * changeSpeed);

            if (mat.color.r >= 1 && mat.color.g >= 1 && mat.color.b >= 1) {
                up = false;
            }
            if (mat.color.r <= 0 && mat.color.g <= 0 && mat.color.b <= 0)
            {
                up = true;
            }
        }

    }
}
