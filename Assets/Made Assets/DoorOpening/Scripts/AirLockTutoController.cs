﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirLockTutoController : MonoBehaviour
{
    public DigicodeController digicode;
    public GameObject tutoGO;
    private bool open = false;

    public void FixedUpdate()
    {
        if (open) return;

        if (digicode.activated)
        {
            if (tutoGO != null)
            {
                Destroy(tutoGO);
            }

            OpenAirLock();
        }
    }

    public void OpenAirLock()
    {
        Animator mAnim = GetComponentInChildren<Animator>();

        if (mAnim.GetCurrentAnimatorStateInfo(0).length >
            mAnim.GetCurrentAnimatorStateInfo(0).normalizedTime) return;

        mAnim.Play("AirLockOpenX", 0, 0);
        mAnim.gameObject.GetComponent<AudioSource>().Play();

        open = true;
    }
}
