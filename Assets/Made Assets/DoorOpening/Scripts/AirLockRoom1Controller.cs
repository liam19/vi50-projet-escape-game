using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirLockRoom1Controller : MonoBehaviour
{
    public DigicodeController digicode;
    public HolderManager fusebox;

    private bool open = false;

    public void FixedUpdate()
    {
        if (open) return;

        if (digicode.activated && fusebox.IsActivated)
        {
            OpenAirLock();
        }
    }

    public void OpenAirLock()
    {
        Animator mAnim = GetComponentInChildren<Animator>();

        if (mAnim.GetCurrentAnimatorStateInfo(0).length >
            mAnim.GetCurrentAnimatorStateInfo(0).normalizedTime) return;

        mAnim.Play("AirLockOpenY", 0, 0);
        mAnim.gameObject.GetComponent<AudioSource>().Play();

        open = true;
    }
}

