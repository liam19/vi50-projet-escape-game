﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class KeyController : MonoBehaviour
{
    private string keyValue;

    private DigicodeController digicodeController;

    public Material keyHitMaterial;
    private Material defaultMaterial;


    // Start is called before the first frame update
    void Start()
    {
        defaultMaterial = GetComponent<Renderer>().material;
        keyValue = GetComponentInChildren<Text>().text;
        digicodeController = transform.parent.GetComponent<DigicodeController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (digicodeController.activated) return; // don't allow using the digicode when already unlocked

        // if button can be pressed, then execute the right action
        if (digicodeController.canHitAgain < Time.time)
        {
            // avoid to hit buttons to fast
            digicodeController.canHitAgain = Time.time + digicodeController.buttonHitAgainTime;

            // case depending on the key hit
            switch (keyValue)
            {
                case "E": // ENTER
                    digicodeController.CheckPassword();
                    break;
                case "C": // CLEAR
                    digicodeController.ClearScreen();
                    GetComponent<Renderer>().material = keyHitMaterial;
                    StartCoroutine(SwitchBackMaterial());
                    break;
                default: // ADD DIGIT
                    digicodeController.AddDigitToScreen(keyValue);
                    GetComponent<Renderer>().material = keyHitMaterial;
                    StartCoroutine(SwitchBackMaterial());
                    break;
            }
        }
    }

    IEnumerator SwitchBackMaterial()
    {
        yield return new WaitForSeconds(0.2f);
        GetComponent<Renderer>().material = defaultMaterial;
    }
}
