﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigicodeController : MonoBehaviour
{
    private const string GRANTED = "Granted";
    private const string DENIED = "Denied";

    public GameObject screen;
    private Text screenTxt;

    public Material validateMaterial;
    public Material errorMaterial;
    private Material defaultMaterial;

    public AudioSource bip;
    public AudioSource validation;
    public AudioSource error;

    public string password = "";

    // output of the digicode
    public bool activated = false;

    // variables used to controll every button when pressed
    public float buttonHitAgainTime { get; }  = 0.5f;
    public float canHitAgain { get; set; } = 0f;


    // Start is called before the first frame update
    void Start()
    {
        defaultMaterial = GetComponent<MeshRenderer>().material;
        screenTxt = screen.GetComponentInChildren<Text>();
        StartCoroutine(DeleteDigitAtStart());
    }

    private IEnumerator DeleteDigitAtStart()
    {
        yield return new WaitForSeconds(1);
        screenTxt.text = "";
    }

    public void CheckPassword()
    {
        if (password.Equals(screenTxt.text))
        {
            ChangeMaterialForSelfAndEachChild(validateMaterial);
            screenTxt.text = GRANTED;
            validation.Play();
            activated = true;
        }
        else
        {
            ChangeMaterialForSelfAndEachChild(errorMaterial);
            screenTxt.text = DENIED;
            error.Play();
            StartCoroutine(SwitchBackMaterial());
        }
    }

    public void AddDigitToScreen(string digit)
    {
        bip.Play();
        if (screenTxt.text.Length < 4)
            screenTxt.text += digit;
    }

    public void ClearScreen()
    {
        screenTxt.text = "";
    }

    private void ChangeMaterialForSelfAndEachChild(Material material)
    {
        GetComponent<Renderer>().material = material;

        foreach (Transform child in transform)
        {
            if (child.name.Equals("Bottom")) continue;
            child.GetComponent<Renderer>().material = material;
        }
    }

    IEnumerator SwitchBackMaterial()
    {
        yield return new WaitForSeconds(1f);
        ChangeMaterialForSelfAndEachChild(defaultMaterial);
        if (screenTxt.text.Equals(DENIED))
        {
            ClearScreen();
        }
    }
}
