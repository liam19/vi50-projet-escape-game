﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Screw : MonoBehaviour
{
    private Vector3 initialPos;
    public float unscrewed;
    public int unscrewSpeed = 5;
    private bool firstUnscrew = true;
    // Start is called before the first frame update
    void Start()
    {
        initialPos = transform.localPosition;
        unscrewed = 0;
    }
    
    public void Unscrew()
    {
        if (unscrewed < 100)
        {
            unscrewed += Time.deltaTime * unscrewSpeed;
            transform.localPosition = initialPos + new Vector3(0, unscrewed * 0.006f, 0);
            transform.localRotation *= Quaternion.Euler(new Vector3(0, 0, -unscrewSpeed * 30 * Time.deltaTime));
        }
        else
        {
            if (firstUnscrew)
            {
                FloatingObject fo = gameObject.AddComponent<FloatingObject>();
                fo.FloatStrenght = 0;
                fo.RandomRotationStrenght = 0.1f;
                firstUnscrew = false;
                gameObject.AddComponent<Grabbable>();
                gameObject.AddComponent<Rigidbody>().useGravity = false;

            }
        }

    }
}
