﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrewBoxManager : MonoBehaviour
{
    public Screw screw1, screw2, screw3, screw4;
    public GameObject lid;
/*    public Transform instancepos;
*/  private bool open = false;
/*    public Transform prefab;*/

    public List<Transform> objectsToInstanciate;
    public List<Transform> placeholders;

    void Update()
    {
        if (!open)
        {
            if (screw1.unscrewed >= 100)
            {
                if (screw2.unscrewed >= 100 && screw3.unscrewed >= 100 && screw4.unscrewed >= 100)
                {
                    lid.AddComponent<Rigidbody>().useGravity = false;
                    lid.AddComponent<Grabbable>();
                    open = true;
                    /*Instantiate(prefab, instancepos.position, Quaternion.identity); // add station as parent*/

                    // instanciate objects inside the screw box
                    for (short i = 0; i < placeholders.Count; i++)
                    {
                        objectsToInstanciate[i].position = placeholders[i].position;
                        objectsToInstanciate[i].GetComponent<FloatingObject>().enabled = true;
                    }
                }
            }
        }
    }
}
