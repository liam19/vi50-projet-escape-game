﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuseboxController : MonoBehaviour
{
    public bool hasFuseUp { set; get; } = false;
    public bool hasFuseBottom { set; get; } = false;

    public bool isActivated()
    {
        return hasFuseUp && hasFuseBottom;
    }
}
