﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FusePlaceholderController : MonoBehaviour
{
    private FuseboxController fuseboxController;
    public Material FuseFilamentOn;

    private GameObject fuse = null;

    private void Start()
    {
        fuseboxController = transform.parent.GetComponent<FuseboxController>();
    }

    private void OnTriggerEnter(Collider other)
    {
         // if not, then place the fuse
        if (other.tag.Equals("Fuse"))
        {
            // if has already a fuse, return
            if (transform.name.Equals("FusePlaceholderU"))
            {
                if (fuseboxController.hasFuseUp) return;

                fuseboxController.hasFuseUp = true;
            }
            else if (transform.name.Equals("FusePlaceholderB"))
            {
                if (fuseboxController.hasFuseBottom) return;
                
                fuseboxController.hasFuseBottom = true;
            }

            fuse = other.gameObject;

            // freeze the fuse
            fuse.GetComponent<Rigidbody>().useGravity = false;
            fuse.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            fuse.GetComponent<OVRGrabbable>().enabled = false;

            // apply the same position and rotatio than the placeholder
            fuse.transform.position = transform.position;
            fuse.transform.rotation = transform.rotation;

            // change the color of the fuse's inside
            fuse.transform.GetChild(2).GetComponent<Renderer>().material = FuseFilamentOn;

            // disable the placeholder
            gameObject.SetActive(false);
        }
    }
}
