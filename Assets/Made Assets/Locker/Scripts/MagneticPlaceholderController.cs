using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticPlaceholderController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Handler" && transform.childCount <= 0)
        {
            // force release and put handler in placeholder
            GameObject.Find("CustomHandRight").GetComponent<Grabber>().ReleaseObject();
            GameObject.Find("CustomHandLeft").GetComponent<Grabber>().ReleaseObject();

            other.transform.SetParent(transform);
            other.transform.localPosition = Vector3.zero;
            other.transform.localRotation = Quaternion.Euler(Vector3.zero);
            other.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
