﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WallTerminalController : MonoBehaviour
{
    enum LedStatus
    {
        ON,
        OFF
    }

    private LedStatus ledStatus = LedStatus.OFF;
    private int fade = 255;

    public Material ledOn;
    public Material ledOff;

    public GameObject led;

    private HolderManager holderManager; // disk manager
    private Text screenText;
    private Image screenImage;

    private void Start()
    {
        holderManager = GetComponentInChildren<HolderManager>();
        screenText = GetComponentInChildren<Text>();
        screenImage = GetComponentInChildren<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        // if disk manager activated, then print message 
        // from disk on screen and update the led
        if (holderManager.IsActivated)
        {
            PrintMessageFromDisk();
            UpdateLedStatusOn();
        }

        // if disk removed after insertion, clear screen and led to blinking red
        if (!holderManager.IsActivated && ledStatus == LedStatus.ON)
        {
            ClearMessage();
            UpdateLedStatusOff();
        }

        // default - blinking red led
        if (ledStatus == LedStatus.OFF)
        {
            BlinkLed();
        }
    }

    private void ClearMessage()
    {
        screenText.text = "";
        screenImage.sprite = null;
        screenImage.enabled = false;
        screenText.enabled = false;
    }

    private void PrintMessageFromDisk()
    {
        // get message from disk
        DiskController disk = holderManager.holders[0].objectHolded.GetComponent<DiskController>();
        if (disk.messageType == DiskController.MessageType.TEXT)
        {
            string message = disk.textMessage;
            // display on screen
            screenText.text = message;
            screenText.enabled = true;
        }
        else if (disk.messageType == DiskController.MessageType.IMAGE)
        {
            Sprite sprite = disk.imageMessage;
            screenImage.sprite = sprite;
            screenImage.enabled = true;
        }
    }

    void BlinkLed()
    {
        Color c = led.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        c.r = fade;
        led.GetComponent<Renderer>().material.SetColor("_EmissionColor", c);

        fade -= 5;

        if (fade <= 0)
        {
            StartCoroutine(Wait());
        }
    }

    private void UpdateLedStatusOn()
    {
        ledStatus = LedStatus.ON;
        led.GetComponent<Renderer>().material = ledOn;
    }

    private void UpdateLedStatusOff()
    {
        ledStatus = LedStatus.OFF;
        led.GetComponent<Renderer>().material = ledOff;
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f);
        fade = 255;
    }

}
