﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiskController : MonoBehaviour
{
    public enum MessageType
    {
        IMAGE,
        TEXT
    }

    public MessageType messageType;

    public string textMessage;

    public Sprite imageMessage;


}
