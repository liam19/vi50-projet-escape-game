﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrateController : MonoBehaviour
{

    private DigicodeController digicode;
    private Animator mAnim;

    public List<Transform> objectsToInstanciate;
    public List<Transform> placeholders;

    private bool open = false;

    // Start is called before the first frame update
    void Start()
    {
        digicode = GetComponentInChildren<DigicodeController>();
        mAnim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (digicode.activated && !open)
        {
            InstanciateObjects();
            Open();
        }
    }

    private void InstanciateObjects()
    {
        // instanciate objects inside the crate
        for (short i = 0; i < placeholders.Count; i++)
        {
            objectsToInstanciate[i].position = placeholders[i].position;
            objectsToInstanciate[i].GetComponent<FloatingObject>().enabled = true;
        }
    }

    public void Open()
    {
        if (mAnim.GetCurrentAnimatorStateInfo(0).length >
            mAnim.GetCurrentAnimatorStateInfo(0).normalizedTime) return;

        mAnim.Play("OpenCrateAnimation", 0, 0);

        open = true;
    }
}
