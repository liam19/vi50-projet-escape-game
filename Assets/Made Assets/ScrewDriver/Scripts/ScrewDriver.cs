﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrewDriver : MonoBehaviour
{
    private Animator headAnimator;
    public Grabber LGrabber, RGrabber;
    private bool screwdriverOn = false;
    private bool inScrew = false;
    private Screw screw;
    // Start is called before the first frame update
    void Start()
    {
        headAnimator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.LTouch) && ReferenceEquals(LGrabber.objectInHand, gameObject)) || (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.RTouch) && ReferenceEquals(RGrabber.objectInHand, gameObject)))
        {
            if (!screwdriverOn)
            {
                StartAnim();
                screwdriverOn = true;
            }
            else
            {
                StopAnim();
                screwdriverOn = false;
            }
        }

        if (inScrew)
        {
            screw.Unscrew();
        }
    }

    private void StartAnim()
    {
        headAnimator.enabled = true;
        gameObject.GetComponent<AudioSource>().Play();
    }
    private void StopAnim()
    {
        headAnimator.enabled = false;
        gameObject.GetComponent<AudioSource>().Stop();
        inScrew = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Screw")
        {
            if (headAnimator.enabled)
            {
                screw = other.GetComponent<Screw>();
                inScrew = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Screw")
        {
            inScrew = false;
        }
    }
}
