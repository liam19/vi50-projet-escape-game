﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVFlashlightController : MonoBehaviour
{

    public Material reveal;
    private Light smartLight;

    private bool activated = false;

    private float spotAngle;
    private float intensity;

    // Start is called before the first frame update
    void Start()
    {
        smartLight = GetComponentInChildren<Light>();

        spotAngle = smartLight.spotAngle;
        intensity = smartLight.intensity;

        smartLight.spotAngle = 0f;
        smartLight.intensity = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMaterial();
    }

    public void switchFlashlight()
    {
        if (activated)
        {
            smartLight.spotAngle = 0f;
            smartLight.intensity = 0f;
        } else
        {
            smartLight.spotAngle = spotAngle;
            smartLight.intensity = intensity;
        }

        activated = !activated;
    }

    private void UpdateMaterial()
    {
        reveal.SetVector("_LightPosition", smartLight.transform.position);
        reveal.SetVector("_LightDirection", -smartLight.transform.forward);
        reveal.SetFloat("_LightAngle", smartLight.spotAngle);
    }
}
