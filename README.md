# VI50 - Projet Escape Game

[Logo](logovr.png)


[FR]


Projet de l'UV VI50.


Escape game en réalité virtuelle.


Pendant un voyage dans votre navette spatiale, suit à un manque de ressources, vous décidez de rejoindre une station spatiale. Mais là, c'est le blackout, et vous vous réveillez désorienté dans la station. L'alarme de la station sonne : vous n'avez que peu de temps pour en sortir, avant que tout explose. Vous devrez faire preuve de sang froid et de logique pour surmonter les épreuves et atteindre votre objectif : rester vivant.


Release : début 2021.


[EN]


School project for VI50, from UTBM.


Escape game in virtual reality.


During a trip in your space shuttle, because a lack of resources, you decide to join a space station. But ou have a memory lapse, and you wake up disoriented in the space station. The station's alarm sounds: you only have a short time to get out, before everything explodes. You will need to use composure and logic to overcome trials and achieve your goal of staying alive.


Release in early 2021.




Credit for the logo : dgim-studio.